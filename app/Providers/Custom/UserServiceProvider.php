<?php

namespace App\Providers\Custom;

use App\Http\Controllers\Api\V1\UserController;
use App\Models\User;
use App\Services\Contracts\ICreate;
use App\Services\Contracts\IDelete;
use App\Services\Contracts\IRead;
use App\Services\GenericImplementations\GenericFileUpload;
use App\Services\Implementations\UserService\UserCreateService;
use App\Services\Implementations\UserService\UserReadService;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserController::class)
            ->needs(ICreate::class)
            ->give(function (){
                $model = new User();
                $fileUploader = $this->app->make(GenericFileUpload::class);
                return new UserCreateService($fileUploader, $model);
            });


        $this->app->when(UserController::class)
            ->needs(IRead::class)
            ->give(function (){
                $model = new User();
                return new UserReadService($model);
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
