<?php

namespace App\Providers\Custom;

use App\Http\Controllers\Api\V1\EventController;
use App\Models\Event;
use App\Services\Contracts\ICreate;
use App\Services\Contracts\IDelete;
use App\Services\Contracts\IRead;
use App\Services\Contracts\IUpdate;
use App\Services\GenericImplementations\GenericFileUpload;
use App\Services\Implementations\EventService\EventCreateService;
use App\Services\Implementations\EventService\EventDeleteService;
use App\Services\Implementations\EventService\EventReadService;
use App\Services\Implementations\EventService\EventUpdateService;
use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(EventController::class)
            ->needs(ICreate::class)
            ->give(function (){
                $model = new Event();
                $fileUploader = $this->app->make(GenericFileUpload::class);
                return new EventCreateService($fileUploader, $model);
            });


        $this->app->when(EventController::class)
            ->needs(IUpdate::class)
            ->give(function (){
                $model = new Event();
                $fileUploader = $this->app->make(GenericFileUpload::class);
                return new EventUpdateService($fileUploader, $model);
            });


        $this->app->when(EventController::class)
            ->needs(IRead::class)
            ->give(function (){
                $model = new Event();
                return new EventReadService($model);
            });

        $this->app->when(EventController::class)
            ->needs(IDelete::class)
            ->give(function (){
                $model = new Event();
                return new EventDeleteService($model);
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
