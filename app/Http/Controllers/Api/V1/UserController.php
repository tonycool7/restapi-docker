<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserLoginRequest;
use App\Services\Contracts\{ICreate, IDelete, IRead, IUpdate};
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class UserController extends Controller
{
    private ICreate $createImplementation;

    private IRead $readImplementation;

    public function __construct(IRead $readImplementation,
                                ICreate $createImplementation
    ){
        $this->readImplementation = $readImplementation;

        $this->createImplementation = $createImplementation;
    }

    /**
     * Display a listing of the resource.
     *
     * @param UserLoginRequest $request
     * @return JsonResponse
     */
    public function login(UserLoginRequest $request) : JsonResponse
    {
        $attributes = $request->all();

        try{
            $response = $this->readImplementation->authenticateUser($attributes);
        }catch (Exception $e){
            dd("oops");
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function logout() : JsonResponse
    {

        $response = $this->readImplementation->logoutUser();

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest $request
     * @return JsonResponse
     */
    public function register(UserCreateRequest $request) : JsonResponse
    {
        $attributes = $request->all();

        $response = $this->createImplementation->create($attributes);

        return response()->json($response, 201);
    }

//    /**
//     * Update the specified resource in storage.
//     *
//     * @param UserCreateRequest $request
//     * @param  int  $id
//     * @return JsonResponse
//     */
//    public function update(UserCreateRequest $request, int $id) : JsonResponse
//    {
//        $attributes = $request->all();
//
//        $response = $this->updateImplementation->update($id, $attributes);
//
//        return response()->json([
//            'data' => $response
//        ], 204);
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  int  $id
//     * @return JsonResponse
//     */
//    public function destroy(int $id) : JsonResponse
//    {
//        $response = $this->deleteImplementation->softDelete($id);
//
//        return response()->json([
//            'data' => $response
//        ], 204);
//    }
}
