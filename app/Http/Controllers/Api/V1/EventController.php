<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventCreateRequest;
use App\Services\Contracts\{ICreate, IDelete, IRead, IUpdate};
use Illuminate\Http\JsonResponse;

class EventController extends Controller
{
    private ICreate $createImplementation;

    private IUpdate $updateImplementation;

    private IDelete $deleteImplementation;

    private IRead $readImplementation;

    public function __construct(IRead $readImplementation,
                                IDelete $deleteImplementation,
                                IUpdate $updateImplementation,
                                ICreate $createImplementation
    ){
        $this->readImplementation = $readImplementation;

        $this->deleteImplementation = $deleteImplementation;

        $this->updateImplementation = $updateImplementation;

        $this->createImplementation = $createImplementation;
    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        $response = $this->readImplementation->getAll();

        return response()->json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EventCreateRequest $request
     * @return JsonResponse
     */
    public function store(EventCreateRequest $request) : JsonResponse
    {
        $attributes = $request->all();

        $response = $this->createImplementation->create($attributes);

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show(int $id) : JsonResponse
    {
        $response = $this->readImplementation->getById($id);

        return response()->json($response, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventCreateRequest $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(EventCreateRequest $request, int $id) : JsonResponse
    {
        $attributes = $request->all();

        $response = $this->updateImplementation->update($id, $attributes);

        return response()->json($response, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy(int $id) : JsonResponse
    {
        $response = $this->deleteImplementation->softDelete($id);

        return response()->json($response, 204);
    }
}
