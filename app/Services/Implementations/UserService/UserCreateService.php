<?php


namespace App\Services\Implementations\UserService;


use Illuminate\Support\Collection;
use \App\Services\GenericImplementations\GenericCreate;
use Illuminate\Support\Facades\Hash;

class UserCreateService extends GenericCreate
{

    public function create(array &$attributes): Collection
    {
        if(in_array( 'photo', array_keys($attributes))){

            $result = $this->fileUploader->upload($attributes['photo'], 'avatar');

            $attributes['photo'] = $result['name'];
        }

        $attributes['password'] = Hash::make($attributes['password']);

        $user = $this->model->create($attributes);

        $token = $user->createToken('event-token');

        $user->token =  $token->plainTextToken;

        return collect($user);
    }
}
