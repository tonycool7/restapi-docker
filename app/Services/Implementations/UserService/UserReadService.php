<?php


namespace App\Services\Implementations\UserService;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use \App\Services\GenericImplementations\GenericRead;


class UserReadService extends GenericRead
{

    public function authenticateUser(array &$params = []) : Model{

        $user = $this->model->where('email', $params['email'])->first();

        if (! $user || !Hash::check($params['password'], $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        //delete all valid tokens before creating a new token
        if(auth()->user()){
            $this->logoutUser();
        }

        $user->token = $user->createToken('event-token')->plainTextToken;

        return $user;
    }

    public function logoutUser() : bool {

        $tokens = auth()->user()->tokens();

        $tokens->delete();

        return true;
    }
}
