<?php


namespace App\Services\Implementations\EventService;


use App\Models\Event;
use \App\Services\GenericImplementations\GenericDelete;

class EventDeleteService extends GenericDelete
{
    public function __construct(Event $model)
    {
        parent::__construct($model);
    }
}
