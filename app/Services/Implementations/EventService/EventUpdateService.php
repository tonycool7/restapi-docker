<?php


namespace App\Services\Implementations\EventService;

use App\Models\Event;
use App\Services\GenericImplementations\GenericFileUpload;
use \App\Services\GenericImplementations\GenericUpdate;

class EventUpdateService extends GenericUpdate
{
    public function __construct(GenericFileUpload $fileUploader, Event $model){
        parent::__construct($fileUploader, $model);
    }
}
