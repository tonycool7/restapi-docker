<?php


namespace App\Services\Implementations\EventService;

use App\Models\Event;
use \App\Services\GenericImplementations\GenericRead;

class EventReadService extends GenericRead
{
    public function __construct(Event $model)
    {
        parent::__construct($model);
    }

}
