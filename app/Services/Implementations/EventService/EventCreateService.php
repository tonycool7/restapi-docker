<?php


namespace App\Services\Implementations\EventService;

use App\Models\Event;
use App\Services\Contracts\IFileUpload;
use \App\Services\GenericImplementations\{GenericCreate};

class EventCreateService extends GenericCreate
{

    public function __construct(IFileUpload $fileUploader, Event $model)
    {
        parent::__construct($fileUploader, $model);
    }
}
