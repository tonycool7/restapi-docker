<?php


namespace App\Services\Contracts;


interface IFileUpload
{

    public function upload($file, string $folderName) : array;

}
