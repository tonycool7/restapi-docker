<?php


namespace App\Services\Contracts;

interface IDelete
{
    public function softDelete(int $id);
}
