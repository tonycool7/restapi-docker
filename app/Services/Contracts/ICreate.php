<?php


namespace App\Services\Contracts;

use Illuminate\Support\Collection;

interface ICreate
{

    public function create(array &$attributes) : Collection;

}
