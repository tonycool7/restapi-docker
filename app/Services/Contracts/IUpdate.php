<?php


namespace App\Services\Contracts;

use Illuminate\Support\Collection;

interface IUpdate
{
    public function update(int $id, array &$attributes) : Collection;
}
