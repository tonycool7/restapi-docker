<?php

namespace App\Services\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface IRead
{

    public function getAll(array &$params = []) : Collection;

    public function getUserContent(array &$params = []) : Collection;

    public function getById(int $id) : Model;

}
