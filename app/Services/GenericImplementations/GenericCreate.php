<?php


namespace App\Services\GenericImplementations;


use App\Services\Contracts\IFileUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use \App\Services\Contracts\ICreate;

class GenericCreate implements ICreate
{
    protected IFileUpload $fileUploader;

    protected Model $model;

    public function __construct(IFileUpload $fileUploader, Model $model){
        $this->model = $model;
        $this->fileUploader = $fileUploader;
    }

    public function create(array &$attributes): Collection
    {
        if(in_array( 'src', array_keys($attributes))){

            if(!in_array('type', array_keys($attributes))) exit('Type attribute is missing');

            $result = $this->fileUploader->upload($attributes['src'], $attributes['type']);

            $attributes['src'] = $result['name'];
        }

        $album = $this->model->create($attributes);

        return collect($album);
    }
}
