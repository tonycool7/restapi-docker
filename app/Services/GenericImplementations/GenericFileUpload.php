<?php


namespace App\Services\GenericImplementations;

use App\Services\Contracts\IFileUpload;

class GenericFileUpload implements IFileUpload
{
    private $images = ['png', 'jpg', 'gif', 'jpeg'];

    private $videos = ['mp4', 'avi'];

    protected function generateUniqueName($prefix) : string{
        return uniqid($prefix);
    }

    private function isAcceptableFileType($fileType, $type) : bool{
        if(!in_array(['image', 'video'], $type)) return false;

        if($type == 'image' && in_array($this->images, $fileType)){
            return true;
        }

        if($type == 'video' && in_array($this->videos, $fileType)){
            return true;
        }

        return false;
    }

    public function upload($file, string $folderName): array
    {
        $ext = $file->getClientOriginalExtension();

        $name = $this->generateUniqueName("{$folderName}-").'.'.$ext;

        $file->storeAs("{$folderName}", $name, 'public');


        return ['name' => $name];
    }

}
