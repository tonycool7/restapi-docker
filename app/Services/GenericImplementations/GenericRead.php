<?php


namespace App\Services\GenericImplementations;

use App\Models\User;
use Illuminate\Database\Eloquent\{Model, Collection};
use App\Services\Contracts\IRead;

abstract class GenericRead implements IRead
{
    protected Model $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function getAll(array &$params = []) : Collection
    {
        try{
            $result = $this->model->all();
        }catch (\Exception $e){
            dd($e);
        }

        return $result;
    }

    public function getUserContent(array &$params = []) : Collection
    {
        try{
            $result = $this->model->where('user_id', auth()->id())->get();
        }catch (\Exception $e){
            dd($e);
        }

        return $result;
    }

    public function getById($id) : Model
    {
        try{
            $result = $this->model->find($id);
        }catch (\Exception $e){
            dd($e);
        }

        return $result;
    }
}
