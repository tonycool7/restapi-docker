<?php


namespace App\Services\GenericImplementations;

use Illuminate\Database\Eloquent\Model;

class GenericPayment
{
    protected $model;

    public function generateOrderId(){
        return uniqid();
    }

    public function getPayment($id){
        return $this->model->where("reference", $id)->first();
    }

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getPaidItemPaidItemTinkoff($request){
        $item = $this->model->where("reference", $request->PaymentId)->first();

        return $item;
    }

    public function getPaidItemPaidItemPaystack($request){
        $item = $this->model->where(['reference' => $request->reference])->first();

        return $item;
    }
}
