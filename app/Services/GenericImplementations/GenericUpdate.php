<?php


namespace App\Services\GenericImplementations;


use App\Services\Contracts\IFileUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use \App\Services\Contracts\IUpdate;

abstract class GenericUpdate implements IUpdate
{
    private Model $model;

    private IFileUpload $fileUploader;

    public function __construct( IFileUpload $fileUploader, Model $model)
    {
        $this->model = $model;

        $this->fileUploader = $fileUploader;
    }

    public function update(int $id, array &$attributes): Collection
    {
        if(in_array( 'resource', array_keys($attributes))){
            $result = $this->fileUploader->upload($attributes['resource'], $attributes['type']);
            $attributes['src'] = $result['name'];
        }

        $album = $this->model->create($attributes);

        return collect($album);
    }
}
