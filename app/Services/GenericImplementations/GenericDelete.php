<?php


namespace App\Services\GenericImplementations;


use Illuminate\Database\Eloquent\Model;
use App\Services\Contracts\IDelete;

abstract class GenericDelete implements IDelete
{

    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function softDelete(int $id)
    {
        $result = false;

        try{
            $row = $this->model->findOrFail($id);

            $result = $row->delete();
        }catch (\Exception $e){
            dd($e);
        }

        return $result;
    }
}
