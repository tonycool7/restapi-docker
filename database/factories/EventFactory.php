<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title,
            'address' => $this->faker->address,
            'country' => $this->faker->country,
            'state' => $this->faker->streetName,
            'type' => 'event',
            'src' => UploadedFile::fake()->image($this->faker->text(10)),
            'start_time' => $this->faker->dateTime('now'),
            'end_time' => $this->faker->dateTime('now'),
            'user_id' => 1
        ];
    }
}
