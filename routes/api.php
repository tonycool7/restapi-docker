<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\V1\{EventController, UserController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [UserController::class, 'login']);

Route::post('/register', [UserController::class, 'register']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test', function(){
    return response()->json([
        'data' => ['apple']
    ]);
});

Route::group(['middleware' => 'auth:sanctum'], function (){
    Route::apiResource('/event', EventController::class);
    Route::apiResource('/user', UserController::class)
        ->except(['store', 'index', 'show', 'create']);

    Route::post('/logout', [UserController::class, 'logout']);
});

Route::fallback(function(){
    return response()->json([
        'data' => 'Page Not Found. If error persists, contact it@events.com'], 404);
});

