<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventServiceTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_event()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $eventData = Event::factory()->raw();

        $eventData['user_id'] = $user['id'];

        $this->post('/v1/event', $eventData)->assertStatus(201);
    }
}
