<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_user_registration()
    {
        $userData = User::factory()->raw();

        $headers = ['Accept' => "application/json"];

        $response = $this->json('POST','/v1/register', $userData, $headers)
            ->assertStatus(201);

        $response->assertJsonStructure([
           'name',
           'email',
           'phone',
            'bio',
            'photo',
            'updated_at',
            'created_at',
            'id',
            'token'
        ]);

        $avatar = $response['photo'];

//        Storage::assertExists("/public/storage/avatar/$avatar");

    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_user_login()
    {
        $userData = User::factory()->create();

        $this->post('/v1/login', [
            'email' => $userData['email'],
            'password' => "password"
        ])->assertStatus(200);

    }
}
